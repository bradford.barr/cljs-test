(ns term.core
    (:require
      [reagent.core :as r]))

(def output (r/atom ()))

(defn log [msg]
  (swap! output conj msg))

(defn main [x]
  (when (< x 10000000)
    (log x)
    (js/setTimeout #(main (+ x 1)) 0)))

;; -------------------------
;; Views

(defn output-component []
  [:div
   (map-indexed (fn [i x] [:pre {:key i} x]) @output)])

(defn run-component []
  [:button {:on-click #(main 0)} "Run"])

(defn clear-component []
  [:button {:on-click #(reset! output ())} "Clear"])

(defn home-page []
  [:div#root
   [run-component]
   [clear-component]
   [output-component]])

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root))
