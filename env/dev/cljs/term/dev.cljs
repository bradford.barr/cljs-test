(ns ^:figwheel-no-load term.dev
  (:require
    [term.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
